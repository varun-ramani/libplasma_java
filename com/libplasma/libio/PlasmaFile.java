package com.libplasma.libio;

/**
 * @author Varun Ramani - 20vr0937@wwprsd.org
 * This class is an upgraded version of RandomAccessFile suitable for easily performing file operations.
 */
import java.io.*;
import java.nio.channels.FileChannel;
public class PlasmaFile extends RandomAccessFile {
    FileOutputStream FOS;
    OutputStreamWriter OSW;
    String path;
    String mode;
    public PlasmaFile(String path, String mode) throws FileNotFoundException{
        super(path, mode);
        this.mode = mode;
        try {
            FOS = new FileOutputStream(path);
            OSW = new OutputStreamWriter(FOS);
            this.seek(0);
        } catch (Exception e) {
            System.out.println("ERROR:");
            e.printStackTrace();
        } 
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getMode() {
        return this.mode;
    }

    public void create() {
        try {
            new File(getPath()).createNewFile();
        } catch (Exception e) {
            
        }
    }
    
    public void close() {
        try {
            OSW.close();
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
    public void write(String towrite) {
        try {
            this.writeBytes(towrite.toString());
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
    public void write(byte[] towrite) {
        try {
            for (byte val : towrite) {
                this.writeByte(val);
            }
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
    public void write(byte towrite) {
        try {
            this.writeByte(towrite);
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
    public void writef(String towrite, Object... args) {
        try {
            this.writeUTF(String.format(towrite, args));
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
    public void writeln(String towrite) {
        try {
            this.writeUTF(towrite + "\n");
        } catch (Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }
    
}
