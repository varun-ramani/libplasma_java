package com.libplasma;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * <h2>Easily Get Input!</h2>
 * Provides useful functionality for getting input from the user.
 * @author Varun Ramani: 20vr0937@wwprsd.org
 */
public class Input {
    /**
     * Utilized to get input.
     * @param prompt Prompt user for input. For instance: "Enter input:"
     * @param a Pass a value of desired return type.
     * @return String
     */
    public static String getInput(String prompt, String a) {
        System.out.print(prompt);
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    /**
     * Utilized to get input.
     * @param prompt Prompt user for input. For instance: "Enter input:"
     * @param a Pass a value of desired return type.
     * @return double
     */
    public static double getInput(String prompt, double a) {
        System.out.print(prompt);
        Scanner in = new Scanner(System.in);
        return in.nextDouble();
    }

    /**
     * Utilized to get input.
     * @param prompt Prompt user for input. For instance: "Enter input:"
     * @param a Pass a value of desired return type.
     * @return long
     */
    public static long getInput(String prompt, long a) {
        System.out.print(prompt);
        Scanner in = new Scanner(System.in);
        return in.nextLong();
    }

    /**
     * Utilized to get input.
     * @param prompt Prompt user for input. For instance: "Enter input:"
     * @param a Pass a value of desired return type.
     * @return int
     */
    public static int getInput(String prompt, int a) {
        System.out.print(prompt);
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }
    
    /**
     *  More feature packed versions of getDBInput
     *
     * @param prompt      Question to prompt the user with
     * @param title       Title of Dialog Box
     * @param typeofboxin The type of Dialog Box wanted: "warning", "error", "question", or "information"
     * @param type Pass a value of desired return type
     * @return String
     */
    public static String getDBInput(String prompt, String title, String typeofboxin, String type) {
        int typeofbox = 10000;
        switch (typeofboxin) {
            case "warning":
                typeofbox = JOptionPane.WARNING_MESSAGE;
                break;
            case "error":
                typeofbox = JOptionPane.ERROR_MESSAGE;
                break;
            case "information":
                typeofbox = JOptionPane.INFORMATION_MESSAGE;
                break;
            case "question":
                typeofbox = 3;
                break;
            default:
                typeofbox = JOptionPane.PLAIN_MESSAGE;

        }
        return JOptionPane.showInputDialog(null, prompt, title, typeofbox);
    }

    /**
     * More feature packed versions of getDBInput
     *
     * @param prompt      Question to prompt the user with
     * @param title       Title of Dialog Box
     * @param typeofboxin The type of Dialog Box wanted: "warning", "error", "question", or "information"
     * @param type Pass a value of desired return type
     * @return double
     */
    public static double getDBInput(String prompt, String title, String typeofboxin, double type) {
        int typeofbox = 10000;
        switch (typeofboxin) {
            case "warning":
                typeofbox = JOptionPane.WARNING_MESSAGE;
                break;
            case "error":
                typeofbox = JOptionPane.ERROR_MESSAGE;
                break;
            case "information":
                typeofbox = JOptionPane.INFORMATION_MESSAGE;
                break;
            case "question":
                typeofbox = 3;
                break;
            default:
                typeofbox = JOptionPane.PLAIN_MESSAGE;

        }
        return Double.parseDouble(JOptionPane.showInputDialog(null, prompt, title, typeofbox));
    }

    /**
     * More feature packed versions of getDBInput
     *
     * @param prompt      Question to prompt the user with
     * @param title       Title of Dialog Box
     * @param typeofboxin The type of Dialog Box wanted: "warning", "error", "question", or "information"
     * @param type Pass a value of desired return type
     * @return int
     */
    public static int getDBInput(String prompt, String title, String typeofboxin, int type) {
        int typeofbox = 10000;
        switch (typeofboxin) {
            case "warning":
                typeofbox = JOptionPane.WARNING_MESSAGE;
                break;
            case "error":
                typeofbox = JOptionPane.ERROR_MESSAGE;
                break;
            case "information":
                typeofbox = JOptionPane.INFORMATION_MESSAGE;
                break;
            case "question":
                typeofbox = 3;
                break;
            default:
                typeofbox = JOptionPane.PLAIN_MESSAGE;

        }
        return Integer.parseInt(JOptionPane.showInputDialog(null, prompt, title, typeofbox));
    }
}
