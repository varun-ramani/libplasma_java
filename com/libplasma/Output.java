package com.libplasma;
import javax.swing.*;

/**
 * <h2>Easily Present Output!</h2>
 * Present the user with output without being forced to use absolutely GIANT statments.
 * @author: Varun Ramani: 20vr0937@wwprsd.org
 */
public class Output {
    /**
     * A wrapper for System.out.println()
     * @param toprint Desired output
     */
    public static void println(Object toprint) {
        System.out.println(toprint);
    }

    /**
     * A wrapper for System.out.print()
     * @param toprint Desired output
     */
    public static void print(Object toprint) {
        System.out.print(toprint);
    }

    /**
     * A wrapper for System.out.printf()
     * @param input String normally passed to printf
     * @param args Variables to interpolate
     */
    public static void printf(String input, Object... args) {
        System.out.printf(input, args);
    }

    /**
     * Basic dialog box output
     *
     * @param message Message to present to the user
     */
    public static void DBOutput(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * A little more feature packed version of DBOutput
     *
     * @param message     Message to present to the user
     * @param title       Title of the Dialog Box
     * @param typeofboxin Type of Dialog Box: "warning", "information", "question", or "error"
     */
    public static void DBOutput(String message, String title, String typeofboxin) {
        int typeofbox = 0;
        switch (typeofboxin) {
            case "warning":
                typeofbox = JOptionPane.WARNING_MESSAGE;
                break;
            case "error":
                typeofbox = JOptionPane.ERROR_MESSAGE;
                break;
            case "information":
                typeofbox = JOptionPane.INFORMATION_MESSAGE;
                break;
            case "question":
                typeofbox = 3;
                break;

        }
        JOptionPane.showMessageDialog(null, message, title, typeofbox);
    }


}
