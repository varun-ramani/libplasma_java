package com.libplasma.libio;
import com.libplasma.Output;
public class PlasmaFileTester {
    public static void main(String[] args) {
        PlasmaFile file = null;
        try {
            file = new PlasmaFile("trollface.txt", "rw");
        } catch (Exception e) {
            Output.println("ERROR: ");
            e.printStackTrace();            
        }
        file.create();
        file.write("Hello");
        file.close();
    }
}
